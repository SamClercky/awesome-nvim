" source from https://github.com/adimit/prolog.vim/blob/master/ftdetect/prolog.vim
autocmd BufNewFile,BufRead *.prolog set filetype=prolog
autocmd BufNewFile,BufRead *.pl set filetype=prolog
autocmd BufNewFile,BufRead *.PRO set filetype=prolog
autocmd BufNewFile,BufRead *.pro set filetype=prolog
