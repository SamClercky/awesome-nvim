local M = {}

function M.setup()
    -- Mappings for nvimtree

    vim.api.nvim_set_keymap(
        "n",
        "<C-n>",
        ":NvimTreeToggle<CR>",
        {
            noremap = true,
            silent = true
        }
    )

    -- Add file icons
    require "nvim-web-devicons".setup {
        override = {
            html = {
                icon = "",
                color = "#DE8C92",
                name = "html"
            },
            css = {
                icon = "",
                color = "#61afef",
                name = "css"
            },
            js = {
                icon = "",
                color = "#EBCB8B",
                name = "js"
            },
            ts = {
                icon = "ﯤ",
                color = "#519ABA",
                name = "ts"
            },
            kt = {
                icon = "󱈙",
                color = "#ffcb91",
                name = "kt"
            },
            png = {
                icon = " ",
                color = "#BD77DC",
                name = "png"
            },
            jpg = {
                icon = " ",
                color = "#BD77DC",
                name = "jpg"
            },
            jpeg = {
                icon = " ",
                color = "#BD77DC",
                name = "jpeg"
            },
            mp3 = {
                icon = "",
                color = "#C8CCD4",
                name = "mp3"
            },
            mp4 = {
                icon = "",
                color = "#C8CCD4",
                name = "mp4"
            },
            out = {
                icon = "",
                color = "#C8CCD4",
                name = "out"
            },
            Dockerfile = {
                icon = "",
                color = "#b8b5ff",
                name = "Dockerfile"
            },
            rb = {
                icon = "",
                color = "#ff75a0",
                name = "rb"
            },
            vue = {
                icon = "﵂",
                color = "#7eca9c",
                name = "vue"
            },
            py = {
                icon = "",
                color = "#a7c5eb",
                name = "py"
            },
            toml = {
                icon = "",
                color = "#61afef",
                name = "toml"
            },
            lock = {
                icon = "",
                color = "#DE6B74",
                name = "lock"
            },
            zip = {
                icon = "",
                color = "#EBCB8B",
                name = "zip"
            },
            xz = {
                icon = "",
                color = "#EBCB8B",
                name = "xz"
            },
            deb = {
                icon = "",
                color = "#a3b8ef",
                name = "deb"
            },
            rpm = {
                icon = "",
                color = "#fca2aa",
                name = "rpm"
            }
        }
    }

    require 'nvim-tree'.setup {
        disable_netrw        = true,
        hijack_netrw         = true,
        --open_on_setup        = false,
        --ignore_ft_on_setup   = {},
        --auto_close           = false,
        auto_reload_on_write = true,
        open_on_tab          = false,
        hijack_cursor        = false,
        update_cwd           = false,
        --update_to_buf_dir    = {
        --    enable = true,
        --    auto_open = true,
        --},
        diagnostics          = {
            enable = false,
            icons = {
                hint = "",
                info = "",
                warning = "",
                error = "",
            }
        },
        update_focused_file  = {
            enable      = false,
            update_cwd  = false,
            ignore_list = {}
        },
        system_open          = {
            cmd  = nil,
            args = {}
        },
        filters              = {
            dotfiles = false,
            custom = {}
        },
        git                  = {
            enable = true,
            ignore = true,
            timeout = 500,
        },
        view                 = {
            width = 30,
            --height = 30,
            hide_root_folder = false,
            side = 'left',
            --auto_resize = false,
            mappings = {
                custom_only = false,
                list = {}
            },
            number = false,
            relativenumber = false,
            signcolumn = "yes"
        },
        trash                = {
            cmd = "trash",
            require_confirm = true
        },
        actions              = {
            change_dir = {
                global = false,
            },
            open_file = {
                quit_on_open = false,
            }
        }
    }

    -- tree folder name , icon color
    vim.cmd("hi NvimTreeFolderIcon guifg = #61afef")
    vim.cmd("hi NvimTreeFolderName guifg = #61afef")
end

return M
