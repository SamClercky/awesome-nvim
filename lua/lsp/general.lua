local shared = require'lsp.shared'

-- Use a loop to conveniently both setup defined servers
-- and map buffer local keybindings when the language server attaches
local servers = {
	"pyls", "tsserver", "texlab", "clangd",
	"html", "svelte"
}

for _, lsp in ipairs(servers) do
	require'lspconfig'[lsp].setup { on_attach = shared.on_attach, capabilities = shared.capabilities, }
end

