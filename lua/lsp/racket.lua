if require("utils").check_lsp_client_active "racket_langserver" then
    return
end

local shared = require'lsp.shared'

require'lspconfig'.racket_langserver.setup{
    on_attach = shared.on_attach,
    capabilities = shared.capabilities,
    filetypes = { "racket", "scheme", "r5rs" },
    settings = {
    }
}
