if require "utils".check_lsp_client_active "tsserver" then
    return
end

-- npm install -g typescript typescript-language-server
-- require'snippets'.use_suggested_mappings()
-- local capabilities = vim.lsp.protocol.make_client_capabilities()
-- capabilities.textDocument.completion.completionItem.snippetSupport = true;
-- local on_attach_common = function(client)
-- print("LSP Initialized")
-- require'completion'.on_attach(client)
-- require'illuminate'.on_attach(client)
-- end
local shared = require("lsp/shared")

require("lspconfig").tsserver.setup {
    --cmd = {
    --    shared.install_path("tsserver/node_modules/typescript/bin/tsserver"),
    --    "--stdio",
    --},
    filetypes = {
        "javascript",
        "javascriptreact",
        "javascript.jsx",
        "typescript",
        "typescriptreact",
        "typescript.tsx",
    },
    on_attach = shared.on_attach,
    -- This makes sure tsserver is not used for formatting (I prefer prettier)
    -- on_attach = require'lsp'.common_on_attach,
    root_dir = require("lspconfig/util").root_pattern("package.json", "tsconfig.json", "jsconfig.json", ".git"),
    settings = { documentFormatting = true },
}
local tsserver_args = {}

if vim.g.tsserver_linter == "eslint" or vim.g.tsserver_linter == "eslint_d" then
    local eslint = {
        lintCommand = vim.g.tsserver_linter .. " -f unix --stdin --stdin-filename   {INPUT}",
        lintStdin = true,
        lintFormats = { "%f:%l:%c: %m" },
        lintIgnoreExitCode = true,
        formatCommand = vim.g.tsserver_linter .. " --fix-to-stdout --stdin  --stdin-filename=${INPUT}",
        formatStdin = true,
    }
    table.insert(tsserver_args, eslint)
end

require("lspconfig").efm.setup {
    -- init_options = {initializationOptions},
    --cmd = { shared.install_path("emf/efm-langserver") },
    init_options = { documentFormatting = true, codeAction = false },
    filetypes = {
        "vue",
        "javascript",
        "javascriptreact",
        "typescript",
        "typescriptreact",
        "javascript.jsx",
        "typescript.tsx",
    },
    settings = {
        rootMarkers = { ".git/", "package.json" },
        languages = {
            vue = tsserver_args,
            javascript = tsserver_args,
            javascriptreact = tsserver_args,
            ["javascript.jsx"] = tsserver_args,
            typescript = tsserver_args,
            ["typescript.tsx"] = tsserver_args,
            typescriptreact = tsserver_args,
        },
    },
}
