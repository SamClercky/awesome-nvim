local shared = {}

local function lsp_keybinds_setup(bufnr)
    local opts = { noremap = true, silent = true, buffer = bufnr }

    -- code lens
    --vim.lsp.codelens.display({
    --    bufnr = bufnr,
    --    client_id = client,
    --}) -- by default show code lens
    vim.keymap.set("n", "<C-space>", vim.lsp.codelens.refresh, opts)
    vim.keymap.set("n", "<leader>lr", vim.lsp.codelens.run, opts)

    -- Mappings.
    vim.keymap.set('n', 'gD', vim.lsp.buf.declaration, opts)
    vim.keymap.set('n', 'gd', vim.lsp.buf.definition, opts)
    vim.keymap.set('n', 'K', vim.lsp.buf.hover, opts)
    vim.keymap.set('n', 'gi', vim.lsp.buf.implementation, opts)
    vim.keymap.set('n', 'gh', vim.lsp.buf.signature_help, opts)
    vim.keymap.set('n', 'gW', vim.lsp.buf.workspace_symbol, opts)
    vim.keymap.set('n', '<leader>wa', vim.lsp.buf.add_workspace_folder, opts)
    vim.keymap.set('n', '<leader>wr', vim.lsp.buf.remove_workspace_folder, opts)
    vim.keymap.set('n', '<leader>wL', function() print(vim.inspect(vim.lsp.buf.list_workspace_folders())) end, opts)
    vim.keymap.set('n', '<leader>we', function() require('lsp_extensions.workspace.diagnostic').set_qf_list() end, opts)
    vim.keymap.set('n', '<leader>D', vim.lsp.buf.type_definition, opts)
    vim.keymap.set('n', '<leader>rn', vim.lsp.buf.rename, opts)
    vim.keymap.set('n', 'gr', vim.lsp.buf.references, opts)
    vim.keymap.set('n', '<space>e', vim.diagnostic.open_float, opts)
    vim.keymap.set('n', '[d', vim.diagnostic.goto_prev, opts)
    vim.keymap.set('n', ']d', vim.diagnostic.goto_next, opts)
    vim.keymap.set('n', '<leader>q', vim.diagnostic.setloclist, opts)
    vim.keymap.set('n', '<leader>ca', vim.lsp.buf.code_action, opts)
    vim.keymap.set('n', 'ss', vim.lsp.buf.document_symbol, opts)

    vim.keymap.set('n', '<leader>xx', "<cmd>Trouble<cr>", opts)
    vim.keymap.set('n', '<leader>xw', "<cmd>Trouble lsp_workspace_diagnostics<cr>", opts)
    vim.keymap.set('n', '<leader>xd', "<cmd>Trouble lsp_document_diagnostics<cr>", opts)
    vim.keymap.set('n', '<leader>xl', "<cmd>Trouble loclist<cr>", opts)
    vim.keymap.set('n', '<leader>xq', "<cmd>Trouble quickfix<cr>", opts)
    vim.keymap.set('n', '<leader>gR', "<cmd>Trouble lsp_references<cr>", opts)

    -- Set some keybinds conditional on server capabilities
    vim.keymap.set("n", "<leader>cf", function() vim.lsp.buf.format { async = true } end, opts)
end

local function lsp_options_setup(bufnr)
    local function buf_set_option(...) vim.api.nvim_buf_set_option(bufnr, ...) end

    buf_set_option('omnifunc', 'v:lua.vim.lsp.omnifunc')
end

local function lsp_setup_highlights(client)
    -- Set autocommands conditional on server_capabilities
    if client.server_capabilities["document_highlight"] then
        -- setup lightbulb for code actions
        vim.fn.sign_define('LightBulbSign', { text = "", texthl = "LspDiagnosticsDefaultInformation" })
        vim.api.nvim_exec([[
        hi LspReferenceRead cterm=bold ctermbg=red guibg=LightYellow
        hi LspReferenceText cterm=bold ctermbg=red guibg=LightYellow
        hi LspReferenceWrite cterm=bold ctermbg=red guibg=LightYellow
        augroup lsp_document_highlight
        autocmd! * <buffer>
            autocmd CursorHold <buffer> lua vim.lsp.buf.document_highlight()
            autocmd CursorMoved <buffer> lua vim.lsp.buf.clear_references()
            autocmd BufWrite,BufEnter,InsertLeave <buffer> lua vim.diagnostic.setloclist({open=false})

            autocmd CursorHold,CursorHoldI <buffer> lua require"nvim-lightbulb".update_lightbulb{sign={text=""}}
        augroup END
            ]], false)
    end
end

function shared.on_attach(client, bufnr)
    require 'lsp_signature'.on_attach() -- better lsp signature

    lsp_keybinds_setup(bufnr)
    lsp_options_setup(bufnr)
    lsp_setup_highlights(client)
end

vim.api.nvim_create_autocmd('LspAttach', {
    group = vim.api.nvim_create_augroup('UserLspConfig', {}),
    callback = function(ev)
        local client = vim.lsp.get_client_by_id(ev.data.client_id)
        shared.on_attach(client, ev.buf)
    end
})

function shared.install_path(server)
    return require("nvim-lsp-installer.settings").current.install_root_dir .. "/" .. server
end

return shared
