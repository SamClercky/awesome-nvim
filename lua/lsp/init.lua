local map = require 'utils'.map
local create_augroup = require "utils".create_augroup
local mk_handler = require "utils".mk_handler

local lsp = {}

function lsp.setup_lsp()
    --vim.cmd [[packadd nvim-lspconfig]]
    require('lspkind').init({})

    local opts = { noremap = true }
    map("n", "<C-j>", ":lnext<cr>", opts)
    map("n", "<C-k>", ":lprev<cr>", opts)
    map("n", "<C-l>", ":cnext<cr>", opts)
    map("n", "<C-h>", ":cprev<cr>", opts)

    create_augroup({
        { 'BufNewFile,BufRead', '/*.c,/*.cpp,/*.objc,/*.objcpp', 'nnoremap <leader>ch :ClangdSwitchSourceHeader<CR>' }
    }, 'LSP_CPP')

    -- replace the default lsp diagnostic letters with prettier symbols
    vim.cmd [[
        sign define DiagnosticSignError text= texthl=DiagnosticSignError linehl= numhl=
        sign define DiagnosticSignWarn text= texthl=DiagnosticSignWarn linehl= numhl=
        sign define DiagnosticSignInfo text= texthl=DiagnosticSignInfo linehl= numhl=
        sign define DiagnosticSignHint text= texthl=DiagnosticSignHint linehl= numhl=
    ]]

    vim.diagnostic.config({
        virtual_text = false,
    })

    --vim.lsp.handlers["textDocument/publishDiagnostics"] = vim.lsp.with(
    --    vim.lsp.diagnostic.on_publish_diagnostics,
    --    {
    --        virtual_text = false,
    --        signs = true,
    --        update_in_insert = false,
    --        underline = true,
    --    }
    --)

    create_augroup({
        { "FileType",    "lspinfo", "nnoremap <silent> <buffer> q :q<CR>" },
        -- Enable autoformatting
        { "BufWritePre", "*",       ":silent lua vim.lsp.buf.format()" },
    }, "_general_lsp")
end

-- lsp utils
function lsp.setup_lsp_utils()
    vim.lsp.handlers['textDocument/codeAction'] = mk_handler(require 'lsputil.codeAction'.code_action_handler)
    vim.lsp.handlers['textDocument/references'] = require 'lsputil.locations'.references_handler
    vim.lsp.handlers['textDocument/definition'] = mk_handler(require 'lsputil.locations'.definition_handler)
    vim.lsp.handlers['textDocument/declaration'] = mk_handler(require 'lsputil.locations'.declaration_handler)
    vim.lsp.handlers['textDocument/typeDefinition'] = mk_handler(require 'lsputil.locations'.typeDefinition_handler)
    vim.lsp.handlers['textDocument/implementation'] = mk_handler(require 'lsputil.locations'.implementation_handler)
    vim.lsp.handlers['textDocument/documentSymbol'] = mk_handler(require 'lsputil.symbols'.document_handler)
    vim.lsp.handlers['workspace/symbol'] = mk_handler(require 'lsputil.symbols'.workspace_handler)
end

return lsp
