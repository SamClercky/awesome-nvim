local shared = require'lsp.shared'

require'lspconfig'.gopls.setup {
    on_attach = shared.on_attach,
    capabilities = shared.capabilities,
    cmd = {shared.install_path("gopls"), "serve"};
    settings = {
        gopls = {
            codelenses = {
                gc_details = true,
                test = true,
                generate = true,
                regenerate_cgo = true,
                tidy = true,
                upgrade_dependency = true,
                vendor = true,
            }
        }
    }
}


