-- Add support for extra lsp features
-- snippets original from rust-tools.nvim

local utils = require("utils")

local M = {}

local function get_params_macro()
    return vim.lsp.util.make_position_params()
end

local latest_buf_id = nil

-- parse the lines from result to get a list of the desirable output
-- Example:
-- // Recursive expansion of the eprintln macro
-- // ============================================

-- {
--   $crate::io::_eprint(std::fmt::Arguments::new_v1(&[], &[std::fmt::ArgumentV1::new(&(err),std::fmt::Display::fmt),]));
-- }
local function parse_lines(t)
    local ret = {}

    local name = t.name
    local text = "// Recursive expansion of the " .. name .. " macro"
    table.insert(ret, "// " .. string.rep("=", string.len(text) - 3))
    table.insert(ret, text)
    table.insert(ret, "// " .. string.rep("=", string.len(text) - 3))
    table.insert(ret, "")

    local expansion = t.expansion
    for string in string.gmatch(expansion, "([^\n]+)") do
        table.insert(ret, string)
    end

    return ret
end

local function handler_macro(_, _, result, _, _, _)
    -- echo a message when result is nil (meaning no macro under cursor) and
    -- exit
    if result == nil then
        vim.api.nvim_out_write("No macro under cursor!\n")
        return;
    end

    -- check if a buffer with the latest id is already open, if it is then
    -- delete it and continue
    utils.delete_buf(latest_buf_id)

    -- create a new buffer
    latest_buf_id = vim.api.nvim_create_buf(false, true) -- not listed and scratch

    -- split the window to create a new buffer and set it to our window
    utils.split(true, latest_buf_id)

    -- set filetpe to rust for syntax highlighting
    vim.api.nvim_buf_set_option(latest_buf_id, "filetype" ,"rust")
    -- write the expansion content to the buffer
    vim.api.nvim_buf_set_lines(latest_buf_id, 0, 0, false, parse_lines(result))

    -- make the new buffer smaller
    utils.resize(true, "-25")
end

-- Sends the request to rust-analyzer to get cargo.tomls location and open it
function M.expand_macro()
    vim.lsp.buf_request(0, "rust-analyzer/expandMacro", get_params_macro(), handler_macro)
end

local function get_params_cargo()
    return {
        textDocument = vim.lsp.util.make_text_document_params(),
    }
end

local function handler_cargo(_, _, result, _, _, _)
    vim.lsp.util.jump_to_location(result)
end

-- Sends the request to rust-analyzer to get cargo.tomls location and open it
function M.open_cargo_toml()
    vim.lsp.buf_request(0, "experimental/openCargoToml", get_params_cargo(), handler_cargo)
end

function M.range_hover()
    local params = vim.lsp.util.make_range_params()
    -- set start and end of selection
    local start_m = vim.api.nvim_buf_get_mark(0, "<")
    local end_m = vim.api.nvim_buf_get_mark(0, ">")
    params.range.start = {
        character = start_m[2],
        line = start_m[1]-1, -- vim starts counting at 1, but lsp at 0
    }
    params.range["end"] = {
        character = end_m[2],
        line = end_m[1]-1, -- vim starts counting at 1, but lsp at 0
    }
    params.position = params.range
    params.range = nil
    --print(vim.inspect(params))

    vim.lsp.buf_request(0, "textDocument/hover", params)
end

return M
