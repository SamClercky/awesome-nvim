local map = require"utils".map
local au = require"utils".create_augroup

require("toggleterm").setup {
	size = 20,
	open_mapping = [[<leader>tt]],
	insert_mappings = false,
	hide_numbers = true,
	direction = "horizontal",
	shading_factor = "0",
	persist_size = true,
	shell = "fish",
	start_in_insert = true,
	close_on_exit = true,
}

map("t", "<C-n>", "<C-\\><C-n>")

--vim.cmd("packadd termdebug")
--
--vim.g.termdebug_wide = 1
--
--map("n", "<leader>dg", ":Gdb<cr>")
--map("n", "<leader>dp", ":Program<cr>")
--map("n", "<leader>ds", ":Source<cr>")
--map("n", "<leader>da", ":Asm<cr>")
--
--map("n", "<leader>db", ":Break<cr>")
--map("n", "<leader>dc", ":Clear<cr>")
--
--au({
--    {"BufNewFile,BufRead,BufEnter", "/*.rs", [[let termdebugger="rust-gdb"]]},
--	{"BufNewFile,BufRead,BufEnter", "/*.c,/*.cpp,/*.h,/*.hpp", [[let termdebugger="gdb"]]},
--}, "TERMINAL")
