local opt = require "utils".opt
local map = require "utils".map
local t = require "utils".t

-- General sets
opt("o", "hidden", true)
opt("w", "numberwidth", 2)
--opt("o", "termguicolors", true)
opt("o", "encoding", "utf-8")
opt("o", "path", vim.o.path .. "**,")
opt("o", "wildignore", "**/node_modules/**")
opt("o", "wildignorecase", true)
opt("o", "incsearch", true)
opt("o", "backup", false)
opt("b", "swapfile", false)
opt("o", "showcmd", true)
opt("o", "compatible", false)
opt("o", "cmdheight", 2)
opt("o", "updatetime", 300)
opt("o", "shortmess", vim.o.shortmess .. "c")
opt("o", "hlsearch", false)
opt("o", "ignorecase", true)
opt("o", "smartcase", true)
opt("o", "gdefault", true)
opt("o", "guifont", "VictorMono Nerd Font:h18")
opt("o", "autoread", true)

opt("w", "number", true)
opt("w", "rnu", true)
opt("w", "spell", false)
opt("w", "signcolumn", "yes")

opt("b", "spelllang", "nl,en")

-- Tabs
opt("b", "tabstop", 4)
opt("b", "softtabstop", 4)
opt("b", "expandtab", true)
opt("o", "smarttab", true)
opt("b", "shiftwidth", 4)
opt("b", "copyindent", true)
opt("b", "autoindent", true)
opt("b", "smartindent", true)

-- Coding handiness
opt("w", "conceallevel", 2)
opt("w", "colorcolumn", "80")
vim.cmd "filetype plugin on"
vim.cmd "filetype plugin indent on"

-- Personal bindings
opt("o", "splitbelow", true)
opt("o", "splitright", true)

map("n", "<leader>h", ":vertical resize +3<cr>")
map("n", "<leader>l", ":vertical resize -3<cr>")
map("n", "<leader>k", ":resize +3<cr>")
map("n", "<leader>j", ":resize -3<cr>")

-- window management
map("n", "<leader>ws", "<C-w><C-s>")
map("n", "<leader>wv", "<C-w><C-v>")

map("n", "<leader>wq", "<C-w><C-q>")
map("n", "<leader>wo", "<C-w><C-o>")

map("n", "<leader>ww", "<C-w><C-w>")
map("n", "<leader>wt", "<C-w><C-t>")
map("n", "<leader>wb", "<C-w><C-b>")

map("n", "<leader>wh", "<C-w><C-h>")
map("n", "<leader>wj", "<C-w><C-j>")
map("n", "<leader>wk", "<C-w><C-k>")
map("n", "<leader>wl", "<C-w><C-l>")

-- Toggle hl search
map("", "<leader>fh", ":set hlsearch!<cr>")
map("n", "<leader>fs", ":up<cr>")
map("n", "<leader>uo", ":up<cr>") -- better for dvorak

-- Move line up and down
map("n", "<A-j>", ":m .+1<CR>==")
map("n", "<A-k>", ":m .-2<CR>==")
map("i", "<A-j>", "<ESC>:m .+1<CR>==gi")
map("i", "<A-k>", "<ESC>:m .-2<CR>==gi")
map("v", "<A-j>", ":m '>+1<CR>gv=gv")
map("v", "<A-k>", ":m '<-2<CR>gv=gv")

-- Add extra undo points
map("i", ",", ",<c-g>u")
map("i", ".", ".<c-g>u")
map("i", "!", "!<c-g>u")
map("i", "?", "?<c-g>u")

-- Cheatsheets
map('n', "<leader>/", ":<C-U>Cheatsheet<cr>")
map('n', "<leader>?", ":Cheat<cr>")

-- hop
map("n", "<leader>s", "<cmd>lua require'hop'.hint_words()<cr>")
map("v", "<leader>s", "<cmd>lua require'hop'.hint_words()<cr>")

-- correct easy spelling mistakes
map("i", "<C-l>", "<c-g>u<Esc>[s1z=`]a<c-g>u", { noremap = true })

-- better yank
map("n", "Y", "y$", { noremap = true })

-- external lsp bindings
map("n", "<leader>lx", "<cmd>LspStop<cr>", { noremap = true })

-- kill all floating windows
map("n", "<C-c><C-c>",
    ":lua for k,v in ipairs(vim.api.nvim_list_wins()) do if vim.api.nvim_win_get_config(v).relative ~= \"\" then vim.api.nvim_win_close(v, {}) end end<cr>")

-- Mouse issues with alacritty
vim.o.mouse = "n"

-- highlights
vim.cmd "hi LineNr guifg=#42464e"
vim.cmd "hi Comment guifg=#42464e"
vim.cmd "hi SignColumn guibg=NONE"
vim.cmd "hi VertSplit guibg=NONE"
vim.cmd "hi DiffAdd guifg=#81A1C1 guibg = none"
vim.cmd "hi DiffChange guifg =#3A3E44 guibg = none"
vim.cmd "hi DiffModified guifg = #81A1C1 guibg = none"
vim.cmd "hi EndOfBuffer guifg=#282c34"
vim.cmd "hi NvimInternalError guifg=#f9929b"

-- blankline
vim.cmd "hi IndentBlanklineChar guifg=#282c34"

-- minimal folds
function _G.MyFoldText()
    local line = vim.fn.getline(vim.v.foldstart)
    local foldlinecount = vim.v.foldend - vim.v.foldend + 1
    return ' ' .. foldlinecount .. ' ' .. line
end

vim.cmd [[
set foldtext=v:lua.MyFoldText()
set fillchars=fold:\
]]
