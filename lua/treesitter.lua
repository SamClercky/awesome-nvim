local ts_config = require("nvim-treesitter.configs")
local opt = require "utils".opt

-- add extra parsers
-- markdwon
--local parser_config = require "nvim-treesitter.parsers".get_parser_configs()
--parser_config.markdown = {
--  install_info = {
--    url = "https://github.com/ikatyang/tree-sitter-markdown", -- local path or git repo
--    files = {"src/parser.c"}
--  },
--  filetype = "markdown", -- if filetype does not agrees with parser name
--}

ts_config.setup {
    ensure_installed = {
        "bash",
        "bibtex",
        "json",
        "svelte",
        "javascript",
        "jsdoc",
        "tsx",
        "typescript",
        "go",
        "yaml",
        "gomod",
        "fish",
        "python",
        "css",
        "html",
        "lua",
        "cpp",
        "rust",
        "toml",
        "c",
        "zig",
        --"markdown"
    },
    highlight = {
        enable = true, -- otherwise conceal does not work
        additional_vim_regex_highlighting = true,
        use_languagetree = true,
        disable = { "kotlin", "latex" }
    },
    autotag = {
        enable = true,
    },
    incremental_selection = {
        enable = true,
        keymaps = {
            init_selection = "gnn",
            node_incremental = "grn",
            scope_incremental = "grc",
            node_decremental = "grm",
        },
    },
    indent = { enable = true },
    textobjects = {
        select = {
            enable = true,
            keymaps = {
                -- You can use the capture groups defined in textobjects.scm
                ["af"] = "@function.outer",
                ["if"] = "@function.inner",
                ["ac"] = "@class.outer",
                ["ic"] = "@class.inner",
                ["ib"] = "@block.inner",
                ["ab"] = "@block.outer",
            },
        },
        swap = {
            enable = true,
            swap_next = {
                ["<leader>a"] = "@parameter.inner",
            },
            swap_previous = {
                ["<leader>A"] = "@parameter.inner",
            },
        },
        move = {
            enable = true,
            set_jumps = true, -- whether to set jumps in the jumplist
            goto_next_start = {
                ["]m"] = "@function.outer",
                ["]]"] = "@class.outer",
            },
            goto_next_end = {
                ["]M"] = "@function.outer",
                ["]["] = "@class.outer",
            },
            goto_previous_start = {
                ["[m"] = "@function.outer",
                ["[["] = "@class.outer",
            },
            goto_previous_end = {
                ["[M"] = "@function.outer",
                ["[]"] = "@class.outer",
            },
        },
        lsp_interop = {
            enable = true,
            peek_definition_code = {
                ["<leader>eu"] = "@function.outer",
                ["<leader>eU"] = "@class.outer",
            },
        },
    },
    rainbow = {
        enable = true,
        extended_mode = false,
        max_file_lines = 1000,
        colors = {
            "#d47d85",
            "#81A1C1",
            "#e7c787",
            "#c882e7",
            "#519ABA",
            "#fca2aa",
            "#a3b8ef",
        },
        termcolors = {
            'Red',
            'Cyan',
            'Yellow',
            'Magenta',
            'Blue',
            'Green',
            'White',
        },
    },
}

-- Enable folding
opt("w", "foldmethod", "expr")
opt("w", "foldexpr", "nvim_treesitter#foldexpr()")
opt("o", "foldlevel", 99)

vim.cmd [[filetype on]]
vim.cmd [[syntax on]]
