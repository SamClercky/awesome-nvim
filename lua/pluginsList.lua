local lazypath = vim.fn.stdpath("data") .. "lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
    vim.fn.system({
        "git",
        "clone",
        "--filter=blob:none",
        "https://github.com/folke/lazy.nvim.git",
        lazypath,
    })
end
vim.opt.rtp:prepend(lazypath)

require("lazy").setup({
    -- deps
    { "nvim-lua/plenary.nvim", lazy = true },
    { "nvim-lua/popup.nvim",   lazy = true },
    "RishabhRD/popfix",

    -- motion
    {
        "smoka7/hop.nvim",
        version = "*",
        opts = {},
    },
    "machakann/vim-sandwich",

    -- Git
    { "tpope/vim-fugitive",          cmd = { 'Git' } },
    {
        "lewis6991/gitsigns.nvim",
        event = "BufRead",
        config = function()
            require "git"
        end
    },


    -- eye candy
    {
        'akinsho/bufferline.nvim',
        tag = "v4.4.1",
        config = function()
            require "bufferline-setup"
        end
    },
    { "chriskempson/base16-vim",     config = function() vim.cmd [[colorscheme base16-default-dark]] end },
    { "norcalli/nvim-colorizer.lua", config = function() require 'colorizer'.setup() end },
    {
        'SamClercky/galaxyline.nvim',
        event = "BufWinEnter",
        branch = 'main',
        config = function()
            require "statusline"
        end
    },

    ---- Neovim LSP Plugins
    {
        "neovim/nvim-lspconfig",
        config = function() require "lsp".setup_lsp() end,
    },
    {
        "williamboman/mason.nvim",
        config = function() require "mason".setup {} end,
        dependencies = { "nvim-lspconfig" },
    },
    {
        "williamboman/mason-lspconfig",
        dependencies = { "mason.nvim" },
        config = function() require "mason-lspconfig".setup {} end
    },
    { "onsails/lspkind-nvim" },
    { "kosayoda/nvim-lightbulb" },
    {
        "RishabhRD/nvim-lsputils",
        config = function() require "lsp".setup_lsp_utils() end
    },
    "ray-x/lsp_signature.nvim",

    {
        "hrsh7th/nvim-cmp",
        config = function() require 'completions' end,
        --event="InsertEnter"
    },
    { "hrsh7th/cmp-nvim-lsp",     event = "InsertEnter", dependencies = { "nvim-cmp" } },
    { "hrsh7th/cmp-buffer",       event = "InsertEnter", dependencies = { "nvim-cmp" } },
    { "hrsh7th/cmp-path",         event = "InsertEnter", dependencies = { "nvim-cmp" } },
    { "hrsh7th/cmp-cmdline",      event = "InsertEnter", dependencies = { "nvim-cmp" } },
    --use { "quangnguyen30192/cmp-nvim-ultisnips", event dependenciessertEnter", after = "nvim-cmp" }
    { 'saadparwaiz1/cmp_luasnip', event = "InsertEnter", dependencies = "nvim-cmp" },

    "simrat39/rust-tools.nvim",
    {
        "folke/trouble.nvim",
        config = function() require "diagnostics" end,
        cmd = { "Trouble" }
    },
    {
        "folke/todo-comments.nvim",
        config = function()
            require "todo-comments".setup {}
        end
    },
    {
        "folke/lsp-colors.nvim",
        config = function()
            require "lsp-colors".setup({
                Error = "#ab4642",
                Warning = "#f7ca88",
                Information = "#7cafc2",
                Hint = "#86c1b9",
            })
        end
    },
    {
        'lervag/vimtex',
        config = function() require 'vimtex' end,
        ft = { 'latex', 'tex', 'bibtex' },
    },
    "liuchengxu/graphviz.vim",

    -- snippets
    --"honza/vim-snippets",
    {
        "L3MON4D3/LuaSnip",
        build = "make install_jsregexp",
        tag = "v2.1.1",
        config = function()
            require "luasnip.loaders.from_vscode".lazy_load()
            require "luasnip.loaders.from_snipmate".lazy_load()
        end,
        dependencies = { "rafamadriz/friendly-snippets" }
    },

    -- Cheat Sheet
    {
        "RishabhRD/nvim-cheat.sh", cmd = { 'Cheat', 'CheatWithoutComments', }
    },
    { "sudormrfbin/cheatsheet.nvim",                 cmd = { 'Cheatsheet', 'CheatsheetEdit' } },

    -- Telescope
    {
        "nvim-telescope/telescope.nvim",
        config = function() require "fzf" end,
        requires = { plenary, popup }
    },
    {
        "nvim-telescope/telescope-media-files.nvim",
        lazy = true,
        config = function()
            require('telescope').load_extension('media_files')
        end
    },
    {
        "nvim-telescope/telescope-ui-select.nvim",
        lazy = true,
        config = function() require('telescope').load_extension('ui-select') end,
    },

    -- Treesitter
    { "nvim-treesitter/nvim-treesitter",             run = ":TSUpdate" },
    { 'nvim-treesitter/nvim-treesitter-textobjects', lazy = true },
    { 'nvim-treesitter/nvim-treesitter-context',     lazy = true },
    { "windwp/nvim-ts-autotag",                      event = "BufRead",                       lazy = true },
    { "p00f/nvim-ts-rainbow",                        event = "BufRead",                       lazy = true },
    {
        "windwp/nvim-autopairs",
        event = "InsertEnter",
        after = "nvim-treesitter",
        config = function()
            require('nvim-autopairs').setup {}
        end,
    },

    -- File explorer
    { "ryanoasis/vim-devicons", lazy = true },
    {
        "nvim-neo-tree/neo-tree.nvim",
        branch = "v3.x",
        dependencies = {
            "nvim-lua/plenary.nvim",
            "nvim-tree/nvim-web-devicons", -- not strictly required, but recommended
            "MunifTanjim/nui.nvim",
        },
        config = function()
            vim.keymap.set("n", "<C-N>", "<cmd>Neotree toggle<cr>")
        end,
    },

    -- Terminal stuff
    {
        "akinsho/nvim-toggleterm.lua",
        event = "BufWinEnter",
        config = function()
            require "terminal"
        end
    },

    ---- debugging
    --use {"mfussenegger/nvim-dap", event="BufWinEnter"}
    --use {"rcarriga/nvim-dap-ui",
    --    event="BufWinEnter",
    --    after="nvim-dap",
    --    config=function() require"dapui".setup{} end,}
    --use {"theHamsta/nvim-dap-virtual-text",
    --    event="BufWinEnter",
    --    after="nvim-dap",}
    --use {"Pocco81/DAPInstall.nvim",
    --    event="BufWinEnter",
    --    after="nvim-dap",
    --    config=function() require"dap-install".setup{} end}
    --end
})

---- check if packer is installed
--local plenary = { "nvim-lua/plenary.nvim" }
--local popup = { "nvim-lua/popup.nvim" }
--local popfix = { "RishabhRD/popfix" }
--
--local install_path = vim.fn.stdpath("data") .. "/site/pack/packer/start/packer.nvim"
--
--if vim.fn.empty(vim.fn.glob(install_path)) > 0 then
--    vim.cmd("!git clone https://github.com/wbthomason/packer.nvim " .. install_path)
--    vim.cmd "packadd packer.nvim"
--end
--
--local packer_exists = pcall(vim.cmd, [[packadd packer.nvim]])
--if not packer_exists then
--    print("Packer not found")
--end
--
--return require "packer".startup(
--    function(use)
--        -- core
--        use { "wbthomason/packer.nvim", opt = true }
--        use { "machakann/vim-sandwich" }
--        use {
--            'phaazon/hop.nvim',
--            as = 'hop',
--            config = function()
--                -- you can configure Hop the way you like here; see :h hop-config
--                require 'hop'.setup { keys = 'etovxqpdygfblzhckisuran' }
--            end
--        }
--        use { 'embear/vim-localvimrc' }
--
--        -- git plugins
--        use { "tpope/vim-fugitive", cmd = { 'Git' } }
--        use { "lewis6991/gitsigns.nvim", event = "BufRead", requires = { plenary },
--            config = function()
--                require "git"
--            end }
--
--        -- eye candy
--        --use { "romgrk/barbar.nvim", event = "BufWinEnter" }
--        use { 'akinsho/bufferline.nvim', tag = "v2.*", config = function() require "bufferline-setup" end }
--        use { "chriskempson/base16-vim", config = function() vim.cmd [[colorscheme base16-default-dark]] end }
--        use { "norcalli/nvim-colorizer.lua", config = function() require 'colorizer'.setup() end }
--        use { 'SamClercky/galaxyline.nvim', event = "BufWinEnter", branch = 'main',
--            config = function()
--                require "statusline"
--            end }
--        use { "lukas-reineke/indent-blankline.nvim",
--            event = "BufRead",
--            setup = function()
--                vim.g.indentLine_enabled = 1
--                vim.g.indent_blankline_char_list = { '▏', '¦', '┆', '┊' }
--
--                vim.g.indent_blankline_filetype_exclude = {
--                    "help",
--                    "terminal",
--                    "dashboard",
--                }
--                vim.g.indent_blankline_buftype_exclude = { "terminal" }
--
--                vim.g.indent_blankline_show_trailing_blankline_indent = false
--                vim.g.indent_blankline_show_first_indent_level = true
--            end }
--
--        ---- Neovim LSP Plugins
--        use { "neovim/nvim-lspconfig",
--            config = function() require "lsp".setup_lsp() end,
--        }
--        use { "williamboman/mason.nvim",
--            config = function() require "mason".setup {} end }
--        use { "williamboman/mason-lspconfig", after = "mason.nvim",
--            config = function() require "mason-lspconfig".setup {} end }
--        use { "onsails/lspkind-nvim" }
--        use { "kosayoda/nvim-lightbulb" }
--        use { "RishabhRD/nvim-lsputils",
--            requires = { popfix },
--            config = function() require "lsp".setup_lsp_utils() end }
--        use { "ray-x/lsp_signature.nvim" }
--
--        use { "hrsh7th/nvim-cmp",
--            config = function() require 'completions' end,
--            --event="InsertEnter"
--        }
--        use { "hrsh7th/cmp-nvim-lsp", event = "InsertEnter", after = "nvim-cmp" }
--        use { "hrsh7th/cmp-buffer", event = "InsertEnter", after = "nvim-cmp" }
--        use { "hrsh7th/cmp-path", event = "InsertEnter", after = "nvim-cmp" }
--        use { "hrsh7th/cmp-cmdline", event = "InsertEnter", after = "nvim-cmp" }
--        --use { "quangnguyen30192/cmp-nvim-ultisnips", event = "InsertEnter", after = "nvim-cmp" }
--        use { 'saadparwaiz1/cmp_luasnip', event = "InsertEnter", after = "nvim-cmp" }
--
--        use { "simrat39/rust-tools.nvim" }
--        --use { "tjdevries/nlua.nvim", ft = { "lua" } }
--        use { "folke/trouble.nvim",
--            config = function() require "diagnostics" end,
--            cmd = { "Trouble" }
--        }
--        use { "folke/todo-comments.nvim", config = function()
--            require "todo-comments".setup {}
--        end }
--        use { "folke/lsp-colors.nvim", config = function()
--            require "lsp-colors".setup({
--                Error = "#ab4642",
--                Warning = "#f7ca88",
--                Information = "#7cafc2",
--                Hint = "#86c1b9",
--            })
--        end }
--        use { 'lervag/vimtex',
--            config = function() require 'vimtex' end,
--            ft = { 'latex', 'tex', 'bibtex' }, }
--        use { "liuchengxu/graphviz.vim" }
--
--        -- snippets
--        --use { "sirver/ultisnips",
--        --    event = "InsertEnter",
--        --    requires = { { "honza/vim-snippets", event = "InsertEnter" },
--        --    } }
--        use { "honza/vim-snippets" }
--        use { "L3MON4D3/LuaSnip",
--            run = "make install_jsregexp", config = function()
--            require "luasnip.loaders.from_vscode".lazy_load()
--            require "luasnip.loaders.from_snipmate".lazy_load()
--        end }
--
--        -- Cheat Sheet
--        use { "RishabhRD/nvim-cheat.sh", cmd = {
--            'Cheat', 'CheatWithoutComments',
--        } }
--        use { "sudormrfbin/cheatsheet.nvim", cmd = { 'Cheatsheet', 'CheatsheetEdit' } }
--
--        -- Telescope
--        use { "nvim-telescope/telescope.nvim",
--            config = function() require "fzf" end,
--            event = "BufWinEnter",
--            requires = { plenary, popup } }
--        use { "nvim-telescope/telescope-media-files.nvim",
--            after = { "telescope.nvim" },
--            config = function()
--                require('telescope').load_extension('media_files')
--            end }
--        use { "nvim-telescope/telescope-ui-select.nvim",
--            after = { "telescope.nvim" },
--            config = function() require('telescope').load_extension('ui-select') end,
--        }
--
--        -- Treesitter
--        use { "nvim-treesitter/nvim-treesitter", run = ":TSUpdate" }
--        use { 'nvim-treesitter/nvim-treesitter-textobjects', after = "nvim-treesitter" }
--        use { 'nvim-treesitter/nvim-treesitter-context', after = "nvim-treesitter" }
--        use { "windwp/nvim-ts-autotag", event = "InsertEnter", after = "nvim-treesitter" }
--        use { "p00f/nvim-ts-rainbow", event = "InsertEnter", after = "nvim-treesitter" }
--        use { "windwp/nvim-autopairs",
--            event = "InsertEnter",
--            after = "nvim-treesitter",
--            config = function()
--                require('nvim-autopairs').setup {}
--            end, }
--
--        -- File explorer
--        use { "kyazdani42/nvim-web-devicons" }
--        use { "ryanoasis/vim-devicons" }
--        use { "kyazdani42/nvim-tree.lua", keys = "<C-n>",
--            config = function() require 'fs'.setup() end }
--
--        -- Terminal stuff
--        use { "akinsho/nvim-toggleterm.lua", event = "BufWinEnter", config = function()
--            require "terminal"
--        end }
--
--        ---- debugging
--        --use {"mfussenegger/nvim-dap", event="BufWinEnter"}
--        --use {"rcarriga/nvim-dap-ui",
--        --    event="BufWinEnter",
--        --    after="nvim-dap",
--        --    config=function() require"dapui".setup{} end,}
--        --use {"theHamsta/nvim-dap-virtual-text",
--        --    event="BufWinEnter",
--        --    after="nvim-dap",}
--        --use {"Pocco81/DAPInstall.nvim",
--        --    event="BufWinEnter",
--        --    after="nvim-dap",
--        --    config=function() require"dap-install".setup{} end}
--    end
--)
