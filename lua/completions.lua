local map = require 'utils'.map
local t = require 'utils'.t

-- must be used to activate autocomplete
vim.o.completeopt = "menu,menuone,noselect"

local has_words_before = function()
    unpack = unpack or table.unpack
    local line, col = unpack(vim.api.nvim_win_get_cursor(0))
    return col ~= 0 and vim.api.nvim_buf_get_lines(0, line - 1, line, true)[1]:sub(col, col):match("%s") == nil
end

-- Setup nvim-cmp.
local luasnip = require "luasnip"
local cmp = require 'cmp'

cmp.setup({
    snippet = {
        -- REQUIRED - you must specify a snippet engine
        expand = function(args)
            --vim.fn["vsnip#anonymous"](args.body) -- For `vsnip` users.
            require('luasnip').lsp_expand(args.body) -- For `luasnip` users.
            --vim.fn["UltiSnips#Anon"](args.body) -- For `ultisnips` users.
            -- require'snippy'.expand_snippet(args.body) -- For `snippy` users.
        end,
    },
    mapping = {
        ['<C-b>'] = cmp.mapping(cmp.mapping.scroll_docs( -4), { 'i', 'c' }),
        ['<C-f>'] = cmp.mapping(cmp.mapping.scroll_docs(4), { 'i', 'c' }),
        ['<C-Space>'] = cmp.mapping(cmp.mapping.complete(), { 'i', 'c' }),
        ['<C-e>'] = cmp.mapping({
            i = cmp.mapping.abort(),
            c = cmp.mapping.close(),
        }),
        --['<S-Tab>'] = cmp.mapping(cmp.mapping.select_prev_item(), { 'i', 's' }),
        --['<Tab>'] = cmp.mapping(cmp.mapping.select_next_item(), { 'i', 's' }),
        ["<Tab>"] = cmp.mapping(function(fallback)
            if cmp.visible() then
                cmp.select_next_item()
                -- You could replace the expand_or_jumpable() calls with expand_or_locally_jumpable()
                -- they way you will only jump inside the snippet region
            elseif luasnip.expand_or_jumpable() then
                luasnip.expand_or_jump()
            elseif has_words_before() then
                cmp.complete()
            else
                fallback()
            end
        end, { "i", "s" }),
        ["<S-Tab>"] = cmp.mapping(function(fallback)
            if cmp.visible() then
                cmp.select_prev_item()
            elseif luasnip.jumpable( -1) then
                luasnip.jump( -1)
            else
                fallback()
            end
        end, { "i", "s" }),
        ['<CR>'] = cmp.mapping.confirm({ select = true }),
    },
    sources = cmp.config.sources({
        { name = 'nvim_lsp' },
        --{ name = 'vsnip' }, -- For vsnip users.
        { name = 'luasnip' }, -- For luasnip users.
        --{ name = 'ultisnips' }, -- For ultisnips users.
        -- { name = 'snippy' }, -- For snippy users.
        { name = 'spell' },
    },
        {
            { name = 'buffer' },
        }
    ),
    formatting = {
        format = function(entry, vim_item)
            vim_item.kind = string.format("%s %s", require("lspkind").presets.default[vim_item.kind], vim_item.kind)
            -- See where 'Text' is coming from in you completion menu
            vim_item.menu = ({
                    nvim_lsp = "ﲳ",
                    nvim_lua = "",
                    treesitter = "",
                    path = "ﱮ",
                    buffer = "﬘",
                    zsh = "",
                    ultisnips = "",
                    spell = "暈",
                })[entry.source.name]

            return vim_item
        end,
    },
})

---- Use buffer source for `/` (if you enabled `native_menu`, this won't work anymore).
--cmp.setup.cmdline('/', {
--    sources = {
--        { name = 'buffer' }
--    }
--})
--
---- Use cmdline & path source for ':' (if you enabled `native_menu`, this won't work anymore).
--cmp.setup.cmdline(':', {
--    sources = cmp.config.sources({
--        { name = 'path' }
--    }, {
--            { name = 'cmdline' }
--        })
--})
--
---- Setup lspconfig.
--local capabilities = require('cmp_nvim_lsp').update_capabilities(vim.lsp.protocol.make_client_capabilities())
---- Replace <YOUR_LSP_SERVER> with each lsp server you've enabled.
--require('lspconfig')['<YOUR_LSP_SERVER>'].setup {
--    capabilities = capabilities
--}
--
--vim.cmd [[autocmd FileType TelescopePrompt lua require('cmp').setup.buffer { enabled = false ]}]]

---- setting up compe
--require'cmp'.setup {
--    enabled = true;
--    autocomplete = true;
--    debug = false;
--    min_length = 1;
--    preselect = 'enable';
--    throttle_time = 80;
--    source_timeout = 200;
--    resolve_timeout = 800;
--    incomplete_delay = 400;
--    max_abbr_width = 100;
--    max_kind_width = 100;
--    max_menu_width = 100;
--    documentation = {
--        border = { '', '' ,'', ' ', '', '', '', ' ' }, -- the border option is the same as `|help nvim_open_win|`
--        winhighlight = "NormalFloat:CompeDocumentation,FloatBorder:CompeDocumentationBorder",
--        max_width = 120,
--        min_width = 60,
--        max_height = math.floor(vim.o.lines * 0.3),
--        min_height = 1,
--    };
--
--    source = {
--        path = true;
--        buffer = true;
--        calc = true;
--        nvim_lsp = true;
--        nvim_lua = true;
--        vsnip = false;
--        ultisnips = true;
--        luasnip = false;
--};}
--
---- setup keybinds
--local opts = {expr=true, silent=true}
--map('i', '<C-Space>', [[compe#complete()]], opts)
--map('i', '<CR>',      [[compe#confirm('<CR>')]], opts)
--map('i', '<C-e>',     [[compe#close('<C-e>')]], opts)
--map('i', '<C-f>',     [[compe#scroll({ 'delta': +4 })]], opts)
--map('i', '<C-d>',     [[compe#scroll({ 'delta': -4 })]], opts)
--
--local check_back_space = function()
--    local col = vim.fn.col('.') - 1
--    return col == 0 or vim.fn.getline('.'):sub(col, col):match('%s') ~= nil
--end
--
---- Use (s-)tab to:
----- move to prev/next item in completion menuone
----- jump to prev/next snippet's placeholder
--_G.tab_complete = function()
--  if vim.fn.pumvisible() == 1 then
--    return t "<C-n>"
----  elseif vim.fn['vsnip#available'](1) == 1 then
----    return t "<Plug>(vsnip-expand-or-jump)"
--  elseif check_back_space() then
--    return t "<Tab>"
--  else
--    return vim.fn['compe#complete']()
--  end
--end
--_G.s_tab_complete = function()
--  if vim.fn.pumvisible() == 1 then
--    return t "<C-p>"
----  elseif vim.fn['vsnip#jumpable'](-1) == 1 then
----    return t "<Plug>(vsnip-jump-prev)"
--  else
--    -- If <S-Tab> is not working in your terminal, change it to <C-h>
--    return t "<S-Tab>"
--  end
--end
--
--opts = {expr=true}
--map("i", "<Tab>", "v:lua.tab_complete()", opts)
--map("s", "<Tab>", "v:lua.tab_complete()", opts)
--map("i", "<S-Tab>", "v:lua.s_tab_complete()", opts)
--map("s", "<S-Tab>", "v:lua.s_tab_complete()", opts)
