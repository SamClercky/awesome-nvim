-- Setup
local opt = { noremap = true, silent = true }
vim.api.nvim_set_keymap("n", "<leader>ff", [[<Cmd>lua require('telescope.builtin').find_files()<cr>]], opt)
vim.api.nvim_set_keymap("n", "<leader>fg", [[<Cmd>lua require('telescope.builtin').live_grep()<cr>]], opt)
vim.api.nvim_set_keymap("n", "<leader>fb", [[<Cmd>lua require('telescope.builtin').buffers()<cr>]], opt)
vim.api.nvim_set_keymap("n", "<leader>bb", [[<Cmd>lua require('telescope.builtin').buffers()<cr>]], opt)
vim.api.nvim_set_keymap("n", "<leader>fh", [[<Cmd>lua require('telescope.builtin').help_tags()<cr>]], opt)
vim.api.nvim_set_keymap("n", "<leader>fl", [[<Cmd>lua require('telescope.builtin').git_files()<cr>]], opt)
vim.api.nvim_set_keymap("n", "<leader>fo", [[<Cmd>lua require('telescope.builtin').oldfiles()<cr>]], opt)
vim.api.nvim_set_keymap("n", "<leader>fm", [[<Cmd>lua require('telescope').extensions.media_files.media_files()<cr>]], opt)

-- Remap commands
vim.api.nvim_set_keymap("n", "z=", [[<Cmd>lua require('telescope.builtin').spell_suggest()<cr>]], opt)

-- Telescope highlights
vim.cmd("hi TelescopeBorder   guifg=#3e4451")
vim.cmd("hi TelescopePromptBorder   guifg=#3e4451")
vim.cmd("hi TelescopeResultsBorder  guifg=#3e4451")
vim.cmd("hi TelescopePreviewBorder  guifg=#525865")
vim.cmd("hi PmenuSel  guibg=#98c379")

require("telescope").setup {
    extensions = {
        ["ui-select"] = {
            require("telescope.themes").get_dropdown {},
        },
    },
}
