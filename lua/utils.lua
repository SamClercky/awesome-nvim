local scopes = {o = vim.o, b = vim.bo, w = vim.wo}

local M = {
}

function M.opt(scope, key, value)
	scopes[scope][key] = value
	if scope ~= "o" then
		scopes["o"][key] = value
	end
end

function M.localmap(mode, lhs, rhs, opts)
	local options = {noremap = true, silent = true}
	if opts then
		options = vim.tbl_extend("force", options, opts)
	end
	vim.api.nvim_buf_set_keymap(0, mode, lhs, rhs, options)
end

function M.map(mode, lhs, rhs, opts)
	local options = {noremap = true, silent = true}
	if opts then
		options = vim.tbl_extend("force", options, opts)
	end
	vim.api.nvim_set_keymap(mode, lhs, rhs, options)
end

function M.create_augroup(aucmds, name)
	local cmd = vim.cmd

	cmd('augroup '..name)
	cmd('autocmd!')
	for _, aucmd in ipairs(aucmds) do
		cmd('autocmd '..table.concat(aucmd, ' '))
	end
	cmd('augroup END')
end

function M.check_lsp_client_active(name)
    local clients = vim.lsp.get_active_clients()
    for _, client in ipairs(clients) do
        if client.name == name then
            return true
        end
    end
    return false
end

function M.t(str)
  return vim.api.nvim_replace_termcodes(str, true, true, true)
end

function M.delete_buf(bufnr)
    if bufnr ~= nil then vim.api.nvim_buf_delete(bufnr, {force = true}) end
end

function M.split(vertical, bufnr)
    local cmd = vertical and "vsplit" or "split"

    vim.cmd(cmd)
    local win = vim.api.nvim_get_current_win()
    vim.api.nvim_win_set_buf(win, bufnr)
end

function M.resize(vertical, amount)
    local cmd = vertical and "vertical resize " or "resize"
    cmd = cmd .. amount

    vim.cmd(cmd)
end

function M.snippet_text_edits_to_text_edits(spe)
    for _, value in ipairs(spe) do
        if value.newText ~= nil then
            value.newText = string.gsub(value.newText, "%$%d", "");
        end
    end
end

function M.is_bufnr_rust(bufnr)
    return vim.api.nvim_buf_get_option(bufnr, 'ft') == 'rust'
end

-- compatibility with lsp handlers
function M.mk_handler(fn)
    return function(...)
        local count = select('#', ...)
        local config_or_client_id = select(4, ...)
        local is_new = type(config_or_client_id) ~= 'number' or count == 4
        if is_new then
            return fn(...)
        else
            local err = select(1, ...)
            local method = select(2, ...)
            local result = select(3, ...)
            local client_id = select(4, ...)
            local bufnr = select(5, ...)
            local config = select(6, ...)
            return fn(err, result, { method = method, client_id = client_id, bufnr = bufnr }, config)
        end
    end
end

return M
