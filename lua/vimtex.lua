local create_augroup = require"utils".create_augroup
local opt = require"utils".opt
local map = require"utils".map

vim.g.tex_flavor="latex"
vim.g.vimtex_view_method="zathura"
vim.g.vimtex_quickfix_mode=0
--vim.g.tex_conceal="abdmg"
vim.g.vimtex_format_enabled=1
vim.g.vimtex_fold_enabled=0

local M = {}

function M.take_screenshot(dir)
    local title = vim.fn.getline("."):match("^%s*(.+)%s*$") -- remove trailing whitespace
    local file_name = title:gsub("%W", "_"):lower()
    local path = vim.b.vimtex.root.."/"..dir.."/"..file_name..".png"
    print("Taking screenshot in 3s: "..path)

    vim.defer_fn(function ()
        -- the -p is here just to bypass the save dialog and wont actially
        -- create a file
        local cmd = "flameshot gui -p \""..path.."\" 2>/dev/null"
        print(cmd)
        os.execute(cmd)

        local payload = string.format([[\begin{figure}[htpb]
    \centering
    \includegraphics[width=0.6\linewidth]{figures/%s.png}
    \caption{%s}
    \label{fig:%s}
\end{figure}
]], file_name, title, file_name)

        local register = "a"
        local prev_reg = vim.fn.getreg(register, 1, 1)
        local prev_mode = vim.fn.getregtype(register)

        -- add payload to document
        vim.fn.setreg(register, payload)
        vim.cmd [[normal V"aP]]
        vim.cmd [[normal V5j=]]

        -- reset register
        vim.fn.setreg(register, prev_reg, prev_mode)

    end, 3000)
end

map("n", "<leader>ls", ":lua require'vimtex'.take_screenshot('figures')<cr>")

create_augroup({
	{'BufNewFile,BufRead', '*.tex', 'set spell'},
	{'BufNewFile,BufRead', '*.tex', 'nnoremap <leader>ct :VimtexTocToggle<cr>'},
}, 'LATEX')

return M
