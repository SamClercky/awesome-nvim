require("bufferline").setup {}

local function map(mode, lhs, rhs, opts)
    local options = { noremap = true, silent = true }
    if opts then
        options = vim.tbl_extends("force", options, opts)
    end
    vim.api.nvim_set_keymap(mode, lhs, rhs, options)
end

-- Move to previous/next
map("n", "<A-,>", ":BufferLineCyclePrev<CR>")
map("n", "<A-.>", ":BufferLineCycleNext<CR>")
---- adapt also for azerty
map("n", "<A-;>", ":BufferLineCycleNext<CR>")

-- Re-order to previous/next
map("n", "<A-<>", ":BufferLineCyclePrev<CR>")
map("n", "<A->>", ":BufferLineCycleNext<CR>")

-- Goto buffer in position...
map("n", "<A-1>", ":BufferLineGoToBuffer 1<CR>")
map("n", "<A-2>", ":BufferLineGoToBuffer 2<CR>")
map("n", "<A-3>", ":BufferLineGoToBuffer 3<CR>")
map("n", "<A-4>", ":BufferLineGoToBuffer 4<CR>")
map("n", "<A-5>", ":BufferLineGoToBuffer 5<CR>")
map("n", "<A-6>", ":BufferLineGoToBuffer 6<CR>")
map("n", "<A-7>", ":BufferLineGoToBuffer 7<CR>")
map("n", "<A-8>", ":BufferLineGoToBuffer 8<CR>")
map("n", "<A-9>", ":BufferLineGoToBuffer 9<CR>")

-- Close buffer
---- QWERTY
map("n", "<A-c>c", ":BufferLineClose<CR>")
map("n", "<A-c><A-c>", ":BufferLineClose<CR>")
map("n", "<A-c>h", ":BufferLineCloseLeft<cr>")
map("n", "<A-c><A-h>", ":BufferLineCloseLeft<cr>")
map("n", "<A-c>l", ":BufferLineCloseRight<cr>")
map("n", "<A-c><A-l>", ":BufferLineCloseRight<cr>")
---- DVORAK
map("n", "<A-q>c", ":BufferClose<CR>")
map("n", "<A-q><A-c>", ":BufferClose<CR>")
map("n", "<A-q>o", ":BufferCloseAllButCurrent<cr>")
map("n", "<A-q><A-o>", ":BufferCloseAllButCurrent<cr>")
map("n", "<A-q>h", ":BufferCloseBuffersLeft<cr>")
map("n", "<A-q><A-h>", ":BufferCloseBuffersLeft<cr>")
map("n", "<A-q>l", ":BufferCloseBuffersRight<cr>")
map("n", "<A-q><A-l>", ":BufferCloseBuffersRight<cr>")

-- Wipeout buffer
--                          :BufferWipeout<CR>
-- Close commands
--                          :BufferCloseAllButCurrent<CR>
--                          :BufferCloseBuffersLeft<CR>
--                          :BufferCloseBuffersRight<CR>
--
-- Magic buffer-picking mode
map("n", "<A-s>", ":BufferLinePick<CR>")

-- Sort automatically by...
map("n", "<Space>bd", ":BufferLineSortByDirectory<CR>")
map("n", "<Space>bl", ":BufferLineSortByExtension<CR>")
