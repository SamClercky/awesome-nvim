local map = require"utils".map

local debugging = {}

function debugging.setup()
    -- create command aliasses used in packer
    vim.cmd [[command! Break lua require'dap'.toggle_breakpoint()<cr>]]
    vim.cmd [[command! Continue lua require'dap'.continue()<cr>]]
    vim.cmd [[command! StepIn lua require'dap'.step_into()<cr>]]
    vim.cmd [[command! StepOver lua require'dap'.step_over()<cr>]]
    vim.cmd [[command! Repl lua require'dap'.repl.open()<cr>]]

    local opts = {noremap=true}
    map("n", "<leader>db", "<cmd>Break<cr>", opts)
    map("n", "<leader>dc", "<cmd>Continue<cr>", opts)
    map("n", "<leader>di", "<cmd>StepIn<cr>", opts)
    map("n", "<leader>do", "<cmd>StepOver<cr>", opts)
    map("n", "<leader>dr", "<cmd>Repl<cr>", opts)
end

function debugging.setup_dap()
    -- create command aliasses used in packer
    vim.cmd [[command! Break lua require'dap'.toggle_breakpoint()<cr>]]
    vim.cmd [[command! Continue lua require'dap'.continue()<cr>]]
    vim.cmd [[command! StepIn lua require'dap'.step_into()<cr>]]
    vim.cmd [[command! StepOver lua require'dap'.step_over()<cr>]]
    vim.cmd [[command! Repl lua require'dap'.repl.open()<cr>]]
end

function debugging.setup_virtual_text()
    vim.g.dap_virtual_text = true
end

return debugging
