vim.g.UltiSnipsExpandTrigger="<C-y>"
vim.g.UltiSnipsJumpForwardTrigger="<c-n>"
vim.g.UltiSnipsJumpBackwardTrigger="<c-i>"

vim.g.UltiSnipsEditSplit="vertical"

--require'snippets'.use_suggested_mappings()
