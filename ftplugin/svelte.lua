if require "utils".check_lsp_client_active "svelte" then
    return
end

require("lspconfig").svelte.setup {
    --cmd = { DATA_PATH .. "/lspinstall/svelte/node_modules/.bin/svelteserver", "--stdio" },
    on_attach = require("lsp.shared").on_attach,
}
