--local python_arguments = {}

---- TODO: replace with path argument
--local flake8 = {
--  LintCommand = "flake8 --ignore=E501 --stdin-display-name ${INPUT} -",
--  lintStdin = true,
--  lintFormats = { "%f:%l:%c: %m" },
--}
--
--local isort = { formatCommand = "isort --quiet -", formatStdin = true }
--
--local yapf = { formatCommand = "yapf --quiet", formatStdin = true }
--local black = { formatCommand = "black --quiet -", formatStdin = true }
--
---- set g:python_linter to chose a linter
--if vim.g.python_linter == "flake8" then
--  table.insert(python_arguments, flake8)
--end
--
--table.insert(python_arguments, isort)

--if not require"utils".check_lsp_client_active "efm" then
--  require("lspconfig").efm.setup {
--    -- init_options = {initializationOptions},
--    cmd = { DATA_PATH .. "/lspinstall/efm/efm-langserver" },
--    init_options = { documentFormatting = true, codeAction = false },
--    filetypes = { "python" },
--    settings = {
--      rootMarkers = { ".git/", "requirements.txt" },
--      languages = {
--        python = python_arguments,
--      },
--    },
--  }
--end

local shared = require("lsp/shared")
if not require "utils".check_lsp_client_active "pyright" then
    -- npm i -g pyright
    require("lspconfig").pyright.setup {
        --cmd = {
        --  shared.install_path("python/node_modules/.bin/pyright-langserver"),
        --  "--stdio",
        --},
        on_attach = require("lsp.shared").on_attach,
        settings = {
            python = {
                analysis = {
                    typeCheckingMode = true,
                    autoSearchPaths = true,
                    useLibraryCodeForTypes = true,
                },
            },
        },
    }
end
