-- globals on every run
vim.g.vimtex_compiler_progname = 'nvr' -- enable synctex
vim.g.tex_flavor = "latex"
vim.g.vimtex_view_method = "zathura"
vim.g.vimtex_quickfix_mode = 0
vim.g.vimtex_format_enabled = 1
vim.g.vimtex_compiler_method = "latexmk"
vim.g.vimtex_fold_enabled = 0

vim.cmd [[set spell]]
-- mappings for tex files
local function map(...) vim.api.nvim_buf_set_keymap(0, ...) end

local opts = { silent = true }
map('n', 'j', 'gj', opts)
map('n', 'k', 'gk', opts)
map('n', '<leader>ct', ':VimtexTocToggle<cr>', { noremap = true })
map('n', '<leader>ll', ':TexlabBuild<cr>', { noremap = true })

-- useful abbr (<buffer> needed, bc non tex files should not have these abbr)
-- words
vim.cmd [[
ab <buffer> Afh Afhankelijk
ab <buffer> afh afhankelijk
ab <buffer> coeff co\"effici\"ent
ab <buffer> coord co\"ordinaten
ab <buffer> dwz d.w.z.
ab <buffer> Eig Eigenschappen
ab <buffer> eig eigenschappen
ab <buffer> hfst hoofdstuk
ab <buffer> Hfst Hoofdstuk
ab <buffer> ifv i.f.v.
ab <buffer> Kooktemp Kooktemperatuur
ab <buffer> kooktemp kooktemperatuur
ab <buffer> Onafh Onafhankelijk
ab <buffer> onafh onafhankelijk
ab <buffer> Smelttemp Smelttemperatuur
ab <buffer> smelttemp smelttemperatuur
ab <buffer> Temp Temperatuur
ab <buffer> temp temperatuur
ab <buffer> tov t.o.v.
ab <buffer> var variabelen
ab <buffer> Vgl Vergelijking
ab <buffer> VN \Opm Vd/Nd
ab <buffer> mmpt massamiddelpunt
]]
---- commands
vim.cmd [[
ab <buffer> dvt \dv[]{}{t}
ab <buffer> dvx \dv[]{}{x}
ab <buffer> dvy \dv[]{}{y}
ab <buffer> dvz \dv[]{}{z}
ab <buffer> pdvt \pdv[]{}{t}
ab <buffer> pdvx \pdv[]{}{x}
ab <buffer> pdvy \pdv[]{}{y}
ab <buffer> pdvz \pdv[]{}{z}
ab <buffer> pdvq \pdv[]{}{q_i}
ab <buffer> pdvqd \pdv[]{}{\dot{q}_i}
ab <buffer> sumi \sum_i
ab <buffer> sumj \sum_j
ab <buffer> sumh \sum_h
ab <buffer> sumk \sum_k
ab <buffer> suml \sum_l
ab <buffer> sumn \sum_n
ab <buffer> intt \int_{t_1}^{t_2}
ab <buffer> int \int_1^2
ab <buffer> intS \int_S
ab <buffer> intV \int_V
ab <buffer> intD \int_\D
ab <buffer> intI \int_{-\infty}^\infty
ab <buffer> intL \int_0^\infty
ab <buffer> \a \alpha
ab <buffer> \b \beta
ab <buffer> \g \gamma
ab <buffer> \G \Gamma
ab <buffer> \d \delta
ab <buffer> \D \Delta
ab <buffer> \z \zeta
ab <buffer> \Z \Zeta
ab <buffer> \o \omega
ab <buffer> \O \Omega
ab <buffer> \s \sigma
ab <buffer> \S \Sigma
ab <buffer> \t \theta
ab <buffer> \T \Theta
ab <buffer> \l \lambda
ab <buffer> \L \Lambda
]]

-- runs only once
if require "utils".check_lsp_client_active "texlab" then
    return
end

local map = require "utils".map
local shared = require("lsp/shared")

require("lspconfig").texlab.setup {
    --cmd = { shared.install_path("latex/texlab") },
    on_attach = shared.on_attach,
    handlers = {
        ["textDocument/publishDiagnostics"] = vim.lsp.with(vim.lsp.diagnostic.on_publish_diagnostics, {
            virtual_text = false,
            signs = true,
            underline = true,
            update_in_insert = true,
        }),
    },
}

-- Compile on initialization, cleanup on quit
vim.api.nvim_exec(
    [[
        augroup vimtex_event_1
            au!
            au User VimtexEventQuit     call vimtex#compiler#clean(0)
            au FocusLost * :wa
        augroup END
    ]],
    false
)
