if require "utils".check_lsp_client_active "cssls" then
    return
end

local capabilities = vim.lsp.protocol.make_client_capabilities()
capabilities.textDocument.completion.completionItem.snippetSupport = true
require "lspconfig".cssls.setup {
    on_attach = require("lsp.shared").on_attach,
    capabilities = capabilities,
}

require "lspconfig".tailwindcss.setup {
    filetypes = { "css", "scss", "less" },
    root_dir = require("lspconfig/util").root_pattern("tailwind.config.js", "postcss.config.ts", ".postcssrc"),
    on_attach = require("lsp.shared").on_attach,
}
