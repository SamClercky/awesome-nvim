if require "utils".check_lsp_client_active "lua_ls" then
    return
end

--local sumneko_root_path = DATA_PATH .. "/lspinstall/lua"
--local sumneko_binary = sumneko_root_path .. "/sumneko-lua-language-server"

local shared = require('lsp/shared')
--local sumneko_root_path = shared.install_path("sumneko_lua/extension/server/bin")
--local sumneko_binary = sumneko_root_path .. "/lua-language-server"

require("lspconfig").lua_ls.setup {
    --on_attach = shared.on_attach,
    --cmd = { sumneko_binary, "-E", sumneko_root_path .. "/main.lua" },
    handlers = {
        ["textDocument/publishDiagnostics"] = vim.lsp.with(vim.lsp.diagnostic.on_publish_diagnostics, {
            virtual_text = false,
            signs = true,
            underline = true,
            update_in_insert = true,
        }),
    },
    settings = {
        Lua = {
            runtime = {
                -- Tell the language server which version of Lua you're using (most likely LuaJIT in the case of Neovim)
                version = "LuaJIT",
                -- Setup your lua path
                path = vim.split(package.path, ";"),
            },
            diagnostics = {
                -- Get the language server to recognize the `vim` global
                globals = { "vim" },
            },
            workspace = {
                -- Make the server aware of Neovim runtime files
                library = {
                    [vim.fn.expand "$VIMRUNTIME/lua"] = true,
                    [vim.fn.expand "$VIMRUNTIME/lua/vim/lsp"] = true,
                },
                maxPreload = 100000,
                preloadFileSize = 1000,
            },
            -- Do not send telemetry data containing a randomized but unique identifier
            telemetry = {
                enable = false,
            },
        },
    },
}

require "utils".create_augroup({
    { "BufWritePre", "*.lua", "lua vim.lsp.buf.format({timeout_ms = 1000})" },
}, "_lua_lsp")
