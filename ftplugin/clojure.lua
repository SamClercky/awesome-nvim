if require "utils".check_lsp_client_active("clojure_lsp") then
    return
end

local shared = require("lsp/shared")

require "lspconfig".clojure_lsp.setup {
}
