-- setup buffer specifcs
local function map(mode, key, cmd)
    vim.api.nvim_buf_set_keymap(0, mode, key, cmd, { noremap = true, silent = true })
end

if require("utils").check_lsp_client_active "rust_analyzer" then
    return
end

local shared = require "lsp.shared"

local opts = {
    tools = { -- rust-tools options
        -- Automatically set inlay hints (type hints)
        autoSetHints = true,

        -- Whether to show the type of an expression when hovering in visual
        -- mode
        -- Requires rust-analyser after 2021-08-02
        hover_range = true,

        runnables = {
            -- whether to use telescope for selection menu or not
            use_telescope = true

            -- rest of the opts are forwarded to telescope
        },

        debuggables = {
            -- whether to use telescope for selection menu or not
            use_telescope = true

            -- rest of the opts are forwarded to telescope
        },

        -- These apply to the default RustSetInlayHints command
        inlay_hints = {

            -- Only show inlay hints for the current line
            only_current_line = true,

            -- Event which triggers a refersh of the inlay hints.
            -- You can make this "CursorMoved" or "CursorMoved,CursorMovedI" but
            -- not that this may cause  higher CPU usage.
            -- This option is only respected when only_current_line and
            -- autoSetHints both are true.
            only_current_line_autocmd = "CursorHold",

            -- wheter to show parameter hints with the inlay hints or not
            show_parameter_hints = true,

            -- prefix for parameter hints
            parameter_hints_prefix = "<- ",

            -- prefix for all the other hints (type, chaining)
            other_hints_prefix = "=> ",

            -- whether to align to the length of the longest line in the file
            max_len_align = false,

            -- padding from the left if max_len_align is true
            max_len_align_padding = 1,

            -- whether to align to the extreme right or not
            right_align = false,

            -- padding from the right if right_align is true
            right_align_padding = 7
        },

        hover_actions = {
            -- the border that is used for the hover window
            -- see vim.api.nvim_open_win()
            border = {
                { "╭", "FloatBorder" }, { "─", "FloatBorder" },
                { "╮", "FloatBorder" }, { "│", "FloatBorder" },
                { "╯", "FloatBorder" }, { "─", "FloatBorder" },
                { "╰", "FloatBorder" }, { "│", "FloatBorder" }
            },

            -- whether the hover action window gets automatically focused
            auto_focus = false
        },

        crate_graph = {
            backend = "x11",
            output = nil,
            full = true
        }
    },
    -- all the opts to send to nvim-lspconfig
    -- these override the defaults set by rust-tools.nvim
    -- see https://github.com/neovim/nvim-lspconfig/blob/master/CONFIG.md#rust_analyzer
    server = {
        --cmd = { shared.install_path("rust/rust-analyzer") },
        on_attach = function(client, bufnr)
            shared.on_attach(client, bufnr)

            map('n', '<leader>lE', ':RustExpandMacro<cr>')
            map('n', '<leader>lC', ':RustOpenCargo<cr>')
            map('n', '<leader>K', ':RustHoverActions<cr>')
            map('v', 'K', [[:lua require"lsp/rust".range_hover()<cr>]])
        end,
        settings = {
            ["rust_analyzer"] = {
                assist = {
                    importGranularity = "module",
                    importPrefix = "by_self",
                },
                cargo = {
                    loadOutDirsFromCheck = true,
                    features = "all",
                },
                procMacro = {
                    enable = true,
                },
            }
        }
    } -- rust-analyer options
}

require('rust-tools').setup(opts)

-- Start LSP
vim.cmd [[LspStart]]
