local shared = require("lsp/shared")
if not require "utils".check_lsp_client_active "prolog_ls" then
    -- npm i -g pyright
    require("lspconfig").prolog_ls.setup {
        on_attach = shared.on_attach,
    }
end
