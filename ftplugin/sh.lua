local shared = require("lsp/shared")

if not require "utils".check_lsp_client_active "bashls" then
    -- npm i -g bash-language-server
    require("lspconfig").bashls.setup {
        --cmd = { shared.install_path("bash/node_modules/.bin/bash-language-server"), "start" },
        on_attach = require("lsp.shared").on_attach,
        filetypes = { "sh", "zsh" },
    }
end

-- sh
local sh_arguments = {}

local shfmt = { formatCommand = "shfmt -ci -s -bn", formatStdin = true }

local shellcheck = {
    LintCommand = "shellcheck -f gcc -x",
    lintFormats = { "%f:%l:%c: %trror: %m", "%f:%l:%c: %tarning: %m", "%f:%l:%c: %tote: %m" },
}

-- enable spellcheck
table.insert(sh_arguments, shellcheck)

if not require "utils".check_lsp_client_active "efm" then
    require("lspconfig").efm.setup {
        -- init_options = {initializationOptions},
        --cmd = { shared.install_path("efm/efm-langserver") },
        init_options = { documentFormatting = true, codeAction = false },
        filetypes = { "sh" },
        settings = {
            rootMarkers = { ".git/" },
            languages = {
                sh = sh_arguments,
            },
        },
    }
end
