if require "utils".check_lsp_client_active "erlangls" then
    return
end

require("lspconfig").erlangls.setup {
    --cmd = { DATA_PATH .. "/lspinstall/elixir/elixir-ls/language_server.sh" },
    on_attach = require "lsp.shared".on_attach,
}
