local opt = require "utils".opt

if require "utils".check_lsp_client_active "clangd" then
    return
end

opt("b", "tabstop", 2)
opt("b", "softtabstop", 2)
opt("b", "shiftwidth", 2)

local clangd_flags = { "--background-index" }

table.insert(clangd_flags, "--cross-file-rename")
table.insert(clangd_flags, "--header-insertion=never")

local shared = require "lsp/shared"

require("lspconfig").clangd.setup {
    --cmd = { shared.install_path("clangd/clangd/bin/clangd"), unpack(clangd_flags) },
    on_attach = require("lsp.shared").on_attach,
}
