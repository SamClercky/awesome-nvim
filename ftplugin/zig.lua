-- autosave is being handled by lsp
vim.g.zig_fmt_autosave = 0


if require("utils").check_lsp_client_active "zls" then
    return
end

-- Vue language server configuration (vetur)
require("lspconfig").zls.setup {}
