if require "utils".check_lsp_client_active "html" then
    return
end

-- npm install -g vscode-html-languageserver-bin
local capabilities = vim.lsp.protocol.make_client_capabilities()
capabilities.textDocument.completion.completionItem.snippetSupport = true
local shared = require("lsp.shared")

require("lspconfig").html.setup {
    --cmd = { shared.install_path("html/node_modules/vscode-langservers-extracted/bin/vscode-html-language-server"), "--stdio" },
    on_attach = shared.on_attach,
    capabilities = capabilities,
}
