if require"utils".check_lsp_client_active "graphql" then
  return
end

-- npm install -g graphql-language-service-cli
require("lspconfig").graphql.setup { on_attach = require("lsp.shared").on_attach }
