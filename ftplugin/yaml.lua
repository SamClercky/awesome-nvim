if require "utils".check_lsp_client_active "yamlls" then
    return
end

local shared = require("lsp/shared")

-- npm install -g yaml-language-server
require("lspconfig").yamlls.setup {
    --cmd = { shared.install_path("yaml/node_modules/.bin/yaml-language-server"), "--stdio" },
    on_attach = require("lsp.shared").on_attach,
}
