## telescope @fuzzy @fzf
Close                                    | <C-C>
Toggle selection                         | <Tab>
Open file in tab                         | <C-T>
Open file in split                       | <C-X>
Open file in vsplit                      | <C-V>
Scroll up in preview window              | <C-U>
Scroll down in preview window            | <C-D>
Send to quickfix list                    | <C-Q>
Send selected items to quickfix list     | <M-Q>

## gitsigns @git
Next hunk                                | ]c
Previous hunk                            | ]c
Hunk text object                         | ih
Stage hunk                               | :Gitsigns stage_hunk, <leader>hs
Reset hunk                               | :Gitsigns reset_hunk, <leader>hr
Stage buffer                             | :Gitsigns stage_buffer, <leader>hS
Undo stage hunk                          | :Gitsigns undo_stage_hunk, <leader>hu
Reset buffer                             | :Gitsigns reset_buffer, <leader>hR
Preview hunk                             | :Gitsigns preview_hunk, <leader>hp
Blame line                               | :lua require"gitsigns".rue}, <leader>hb
Toggle current blame                     | :Gitsigns toggle_current_line_blame, <leader>tb
Diff this                                | :Gitsigns diffthis, <leader>hd
Diff home dir                            | :lua require"gitsigns".diffthis("~"), <leader>hD
Toggle deleted                           | :Gitsigns toggle_deleted, <leader>td

## sandwich @surround
Add                                      | sa<char>
Delete                                   | sd<char>
Delete (detect surround char)            | sdb
Replace                                  | sr<before><after>
Replace (detect surround char)           | srb<after>
Text object (detect surround char)       | ib, ab
Surround with function                   | sa<motion/txtobj>f
Delete surrounding function              | sdf
Delete outer nested function             | sdF
Surround with word (Tab to autocomplete) | sa<motion/txtobj>i
Delete word (accepts regex)              | sdi
Add HTML tag                             | sa<motion/txtobj>t
Delete HTML tag                          | sdt
Replace HTML tag only                    | srtt
Replace HTML tag and attributes          | srTT

## lsp @personal-lsp
Lsp declaration                          | gD
Lsp definition                           | gd 
Lsp hover                                | K
Lsp implementation                       | gi
Lsp signature_help                       | gh
Lsp workspace symbol                     | gW
Lsp add workspace folder                 | <leader>wa
Lsp remove workspace_folder              | <leader>wr
Lsp list workspace folders               | <leader>wL
Lsp type definition                      | <leader>D
Lsp rename                               | <leader>rn
Lsp references                           | gr
Lsp show_line_diagnostics                | <space>e
Lsp goto_prev                            | [d
Lsp goto_next                            | ]d
Lsp set_loclist                          | <leader>q
Lsp workspace diagnostics                | <leader>we
Lsp code_action                          | <leader>ca
Lsp document_symbol                      | ss
Lsp formatting                           | <leader>cf
Lsp locallist next                       | :lnext, <C-j>
Lsp locallist prev                       | :lprev, <C-k>
Lsp quicklist next                       | :cnext, <C-l>
Lsp quicklist prev                       | :cprev, <C-h>

## Treesitter @treesitter
Init selection                           | gnn
Init selection                           | gnn
Node incremental                         | grn
Scope incremental                        | grc
Node decremental                         | grm
Textobject @function.outer               | af
Textobject @function.inner               | if
Textobject @class.outer                  | ac
Textobject @class.inner                  | ic
Textobject swap next                     | <leader>a
Textobject swap prev                     | <leader>A
Peek definition @function.outer          | <leader>eu
Peek definition @class.outer             | <leader>eU

## Debugging @term-debug
Go to Gdb window                         | <leader>dg, :Gdb
Go to program output                     | <leader>dp, :Program
Go to Source                             | <leader>ds, :Source
Disassemble code                         | <leader>da, :Asm
Add breakpoint                           | <leader>db, :Break
Clear breakpoint                         | <leader>dc, :Clear

## Vimtex @latex @lsp
Toggle Compilation mode                  | :VimtexCompile, <leader>ll
Fzf find                                 | :call vimtex#fzf#run()
Project Information                      | :VimtexInfo
Stop Project Compilation                 | :VimtexStop
Toggle TOC                               | :VimtexTocToggle, <leader>ct
View PDF                                 | :VimtexView, <leader>lv
