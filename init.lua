-- global vars
CONFIG_PATH = vim.fn.stdpath "config"
DATA_PATH = vim.fn.stdpath "data"
CACHE_PATH = vim.fn.stdpath "cache"
TERMINAL = vim.fn.expand "$TERMINAL"
USER = vim.fn.expand "$USER"
HOME = vim.fn.expand "$HOME"

-- REALLY IMPORTANT
vim.g.mapleader = " "
vim.g.maplocalleader = " "

vim.o.runtimepath = vim.o.runtimepath .. ",~/.vim"

vim.o.termguicolors = 1

-- load plugins
require("pluginsList")
require("ultisnips")
require("lsp/shared")
--require("bufferline")
require("treesitter")
require("fs")
require("debugging").setup()
require("personalsettings")
