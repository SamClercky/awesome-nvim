silent! syntax clear rustOperator

" Matches x0 -> x₀ A2 -> A₂ word2 -> word₂
" Use ms=s+1 to avoid concealing the letter before the number
syntax match Normal '\v<[[:alpha:]_]+0>'ms=e conceal cchar=₀
syntax match Normal '\v<[[:alpha:]_]+1>'ms=e conceal cchar=₁
syntax match Normal '\v<[[:alpha:]_]+2>'ms=e conceal cchar=₂
syntax match Normal '\v<[[:alpha:]_]+3>'ms=e conceal cchar=₃
syntax match Normal '\v<[[:alpha:]_]+4>'ms=e conceal cchar=₄
syntax match Normal '\v<[[:alpha:]_]+5>'ms=e conceal cchar=₅
syntax match Normal '\v<[[:alpha:]_]+6>'ms=e conceal cchar=₆
syntax match Normal '\v<[[:alpha:]_]+7>'ms=e conceal cchar=₇
syntax match Normal '\v<[[:alpha:]_]+8>'ms=e conceal cchar=₈
syntax match Normal '\v<[[:alpha:]_]+9>'ms=e conceal cchar=₉

" Numbers
syntax match Normal '\v[^_]\zs_0\ze>' conceal cchar=₀
syntax match Normal '\v[^_]\zs_1\ze>' conceal cchar=₁
syntax match Normal '\v[^_]\zs_2\ze>' conceal cchar=₂
syntax match Normal '\v[^_]\zs_3\ze>' conceal cchar=₃
syntax match Normal '\v[^_]\zs_4\ze>' conceal cchar=₄
syntax match Normal '\v[^_]\zs_5\ze>' conceal cchar=₅
syntax match Normal '\v[^_]\zs_6\ze>' conceal cchar=₆
syntax match Normal '\v[^_]\zs_7\ze>' conceal cchar=₇
syntax match Normal '\v[^_]\zs_8\ze>' conceal cchar=₈
syntax match Normal '\v[^_]\zs_9\ze>' conceal cchar=₉

" Letters
syntax match Normal '\v[^_]\zs_[aA]\ze>' conceal cchar=ₐ
syntax match Normal '\v[^_]\zs_[lL]\ze>' conceal cchar=ₗ
syntax match Normal '\v[^_]\zs_[pP]\ze>' conceal cchar=ₚ
syntax match Normal '\v[^_]\zs_[rR]\ze>' conceal cchar=ᵣ
syntax match Normal '\v[^_]\zs_[sS]\ze>' conceal cchar=ₛ
syntax match Normal '\v[^_]\zs_[uU]\ze>' conceal cchar=ᵤ
syntax match Normal '\v[^_]\zs_[vV]\ze>' conceal cchar=ᵥ
syntax match Normal '\v[^_]\zs_[xX]\ze>' conceal cchar=ₓ	
syntax match Normal '\v[^_]\zs_[hH]\ze>' conceal cchar=ₕ
syntax match Normal '\v[^_]\zs_[iI]\ze>' conceal cchar=ᵢ
syntax match Normal '\v[^_]\zs_[jJ]\ze>' conceal cchar=ⱼ
syntax match Normal '\v[^_]\zs_[kK]\ze>' conceal cchar=ₖ
syntax match Normal '\v[^_]\zs_[nN]\ze>' conceal cchar=ₙ
syntax match Normal '\v[^_]\zs_[mM]\ze>' conceal cchar=ₘ
syntax match Normal '\v[^_]\zs_[tT]\ze>' conceal cchar=ₜ

" " Conceal underscores in numeric literals with spaces
" syntax match Constant '\v<\d+\zs_\ze\d+>' conceal cchar=,

" Conceal things like a_ -> a'
syntax match Normal '\v[^_]\zs_\ze>' conceal cchar= 
" Underscore by itself is not concealed
syntax match Normal '\v<\zs_\ze>' conceal cchar=_

syntax match Normal '\s@\s'ms=s+1,me=e-1 conceal cchar=⊗
syntax match Normal '\s\*\s'ms=s+1,me=e-1 conceal cchar=∙
" syntax match Normal '\v(\+|-|*|/|\%)@!\=' conceal cchar=←
syntax match Normal '\v[^-=+*/!<>]\zs\=\ze[^=<>]' conceal cchar=←
syntax match Normal '\v\=@<!\=\=\=@!' conceal cchar=≝

syntax match Normal "INFINITY" conceal cchar=∞
syntax match Normal 'NEG_INFINITY' conceal cchar=⧜

syntax match Normal '\v\zs ?\*\* ?2\ze>([^.]|$)' conceal cchar=²
syntax match Normal '\v\zs ?\*\* ?n\ze>([^.]|$)' conceal cchar=ⁿ
syntax match Normal '\v\zs ?\*\* ?i\ze>([^.]|$)' conceal cchar=ⁱ	
syntax match Normal '\v\zs ?\*\* ?j\ze>([^.]|$)' conceal cchar=ʲ
syntax match Normal '\v\zs ?\*\* ?k\ze>([^.]|$)' conceal cchar=ᵏ
syntax match Normal '\v\zs ?\*\* ?t\ze>([^.]|$)' conceal cchar=ᵗ
syntax match Normal '\v\zs ?\*\* ?x\ze>([^.]|$)' conceal cchar=ˣ
syntax match Normal '\v\zs ?\*\* ?y\ze>([^.]|$)' conceal cchar=ʸ
syntax match Normal '\v\zs ?\*\* ?z\ze>([^.]|$)' conceal cchar=ᶻ
syntax match Normal '\v\zs ?\*\* ?a\ze>([^.]|$)' conceal cchar=ᵃ
syntax match Normal '\v\zs ?\*\* ?b\ze>([^.]|$)' conceal cchar=ᵇ
syntax match Normal '\v\zs ?\*\* ?c\ze>([^.]|$)' conceal cchar=ᶜ
syntax match Normal '\v\zs ?\*\* ?d\ze>([^.]|$)' conceal cchar=ᵈ
syntax match Normal '\v\zs ?\*\* ?e\ze>([^.]|$)' conceal cchar=ᵉ
syntax match Normal '\v\zs ?\*\* ?p\ze>([^.]|$)' conceal cchar=ᵖ
syntax match Normal '\v\zs ?\*\* ?l\ze>([^.]|$)' conceal cchar=ˡ
syntax match Normal '\v\zs ?\*\* ?m\ze>([^.]|$)' conceal cchar=ᵐ

syntax keyword Normal alpha conceal cchar=α
syntax keyword Normal beta conceal cchar=β
syntax keyword Normal Gamma conceal cchar=Γ
syntax keyword Normal gamma conceal cchar=γ
syntax keyword Normal Delta conceal cchar=Δ
syntax keyword Normal delta conceal cchar=δ
syntax keyword Normal epsilon conceal cchar=ε
syntax keyword Normal zeta conceal cchar=ζ
syntax keyword Normal eta conceal cchar=η
syntax keyword Normal Theta conceal cchar=ϴ
syntax keyword Normal theta conceal cchar=θ
syntax keyword Normal kappa conceal cchar=κ
syntax keyword Normal lambda lambda_ _lambda conceal cchar=λ
syntax keyword Normal mu conceal cchar=μ
syntax keyword Normal nu conceal cchar=ν
syntax keyword Normal Xi conceal cchar=Ξ
syntax keyword Normal xi conceal cchar=ξ
syntax keyword Normal pi conceal cchar=π
syntax keyword Normal Pi conceal cchar=Π
syntax keyword Normal rho conceal cchar=ρ
syntax keyword Normal sigma conceal cchar=σ
syntax keyword Normal tau conceal cchar=τ
syntax keyword Normal upsilon conceal cchar=υ
syntax keyword Normal Phi conceal cchar=Φ
syntax keyword Normal phi conceal cchar=φ
syntax keyword Normal chi conceal cchar=χ
syntax keyword Normal Psi conceal cchar=Ψ
syntax keyword Normal psi conceal cchar=ψ
syntax keyword Normal Omega conceal cchar=Ω
syntax keyword Normal omega conceal cchar=ω
syntax keyword Normal nabla conceal cchar=∇

" Need to be handled specially for `not in` to work. Order doesn't matter.
syntax match Normal '\v<not in>' conceal cchar=∉
syntax match Normal '\v<in>' conceal cchar=∈

syntax keyword Normal Some conceal cchar=⚉
syntax keyword Constant None conceal cchar=∅
syntax keyword Constant true conceal cchar=⊤
syntax keyword Constant false conceal cchar=⊥

" http://www.fileformat.info/info/unicode/block/geometric_shapes/images.htm
syntax keyword Keyword break conceal cchar=◁
syntax keyword Keyword continue conceal cchar=↻
syntax keyword Keyword return conceal cchar=◀
syntax keyword Keyword if conceal cchar=▸
syntax keyword Keyword elif conceal cchar=▹
syntax keyword Keyword else conceal cchar=▪

syntax keyword Normal for conceal cchar=∀
syntax keyword Normal while conceal cchar=⥁
syntax keyword Normal loop conceal cchar=↬

syntax keyword Normal fn conceal cchar=λ
syntax keyword Normal struct conceal cchar=
syntax keyword rsKeyword unwrap expect conceal cchar=‽
syntax keyword rsKeyword self Self  conceal cchar=
syntax keyword Normal "\vmap_?" conceal cchar=ℱ

syntax keyword Builtin all
syntax keyword Builtin any conceal cchar=∃

highlight! link rsBuiltin rsOperator
highlight! link rsOperator Operator
highlight! link rsStatement Statement
highlight! link rsKeyword Keyword
highlight! link rsComment Comment
highlight! link rsConstant Constant
highlight! link rsSpecial Special
highlight! link rsIdentifier Identifier
highlight! link rsType Type
